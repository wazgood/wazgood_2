package ahextech.com.whatsgood.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import ahextech.com.whatsgood.R;
import ahextech.com.whatsgood.fragments.Trending;
import ahextech.com.whatsgood.holders.TrendringViewHolder;
import ahextech.com.whatsgood.models.TrendringList;

/**
 * Created by androiddeveloper on 6/5/16.
 */
public class TrendringAdapter extends RecyclerView.Adapter<TrendringViewHolder>{
    private ArrayList<TrendringList> android;
    private Trending context;
    LayoutInflater vi;
    int Resource;
    public TrendringAdapter(Trending context, int resource, ArrayList<TrendringList> android) {
        this.android = android;
        this.context = context;    //deviceName,description,item_cost,free_delivery,delivery_cost,offer_price,is_active
        vi = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
    }

    @Override
    public TrendringViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = vi.inflate(Resource, null);
        TrendringViewHolder rcv = new TrendringViewHolder(layoutView,(TrendringViewHolder.MyViewHolder) context);
        //TechnologiesViewHolder tvh = new TechnologiesViewHolder(layoutView,(TechnologiesViewHolder.MyViewHolder) context);
        return rcv;

    }

    @Override
    public void onBindViewHolder(TrendringViewHolder holder, int position) {

       /* Picasso.with(context.getContext())
                .load(""+android.get(position).getLink())
                .placeholder(R.drawable.load)

                .into(holder.displayImage);
*/


        try {
            // Start the MediaController
            /*MediaController mediacontroller = new MediaController(context.getActivity());
            mediacontroller.setAnchorView(holder.displayVideo);*/
            // Get the URL from String VideoURL
            Uri video = Uri.parse(android.get(position).getResource());
            /*holder.displayVideo.setMediaController(mediacontroller);
            holder.displayVideo.setVideoURI(video);
            holder.displayVideo.start();*/


            Picasso.with(context.getContext())
                    .load(""+android.get(position).getImageLink())
                    .placeholder(R.drawable.loading)
                    .into(holder.displayVideo);

            Picasso.with(context.getContext())
                    .load("https://graph.facebook.com/" + android.get(position).getPost_id()+ "/picture?type=large")
                    .placeholder(R.drawable.loading)
                    .into(holder.display_profilepic);

            holder.profile_name.setText(android.get(position).getPost_name());
            holder.post_file_address.setText(android.get(position).getAddress());
            String s=  android.get(position).getCreated_at();
            String trim = s.substring(s.indexOf(" ")+1);
            holder.commints.setText(trim+"\n"+android.get(position).getComments());
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return this.android.size();
    }
}
