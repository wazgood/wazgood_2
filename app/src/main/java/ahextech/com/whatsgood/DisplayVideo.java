package ahextech.com.whatsgood;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import ahextech.com.whatsgood.fragments.Trending;
import ahextech.com.whatsgood.holders.TrendringViewHolder;
import ahextech.com.whatsgood.models.TrendringList;

public class DisplayVideo extends AppCompatActivity
{
    TrendringList tl;
    VideoView videoview;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_video);
        videoview = (VideoView) findViewById(R.id.VideoView);
        Bundle extras = getIntent().getExtras();
        tl = (TrendringList) extras.getSerializable("item");

        Toast.makeText(getApplicationContext(),tl.getResource(),Toast.LENGTH_SHORT).show();
        Log.d("AHEX",""+tl.getResource());


        pDialog = new ProgressDialog(DisplayVideo.this);
        // Set progressbar title
       // pDialog.setTitle("Please wait......");
        // Set progressbar message
        pDialog.setMessage("Buffering...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    DisplayVideo.this);
            mediacontroller.setAnchorView(videoview);
            // Get the URL from String VideoURL
            Log.d("AJJE",tl.getResource());
            Uri video = Uri.parse(tl.getResource());
            videoview.setMediaController(mediacontroller);

            videoview.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoview.start();
            }
        });

    }

    public void gotoHomefromDiplayVideo(View view)
    {
        Intent intent = new Intent(this, HomePage.class);
        startActivity(intent);
    }
}
