package ahextech.com.whatsgood;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Decision extends AppCompatActivity {
    public String ans1,ans2,ans3,ans4;
    ProgressDialog dialog;
    int flag;
    Button showMap;
    TextView doAction,displaySelected;
    TextView namer;
    ImageView imageView;
    LinearLayout ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decision);
        Bundle bundle = getIntent().getExtras();
        ans2 = bundle.getString("Weather");
        ans3 = bundle.getString("Budget");
        ans1 = bundle.getString("Mood");
        ans4 = bundle.getString("Age");
        showMap = (Button)findViewById(R.id.showMap);
        showMap.setVisibility(View.INVISIBLE);
        ll = (LinearLayout)findViewById(R.id.laoutDisplay);
        ll.setVisibility(View.INVISIBLE);
        doAction = (TextView) findViewById(R.id.doaction);
        displaySelected = (TextView) findViewById(R.id.selected);
        namer = (TextView) findViewById(R.id.nameText);
        imageView = (ImageView) findViewById(R.id.imagere);
        //alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

        dialog = new ProgressDialog(Decision.this, AlertDialog.THEME_HOLO_DARK);
        //dialog.setProgressStyle(BIND_ADJUST_WITH_ACTIVITY);

        dialog.setMessage("What's Good......");


        new Task().execute("Hello");
        //Toast.makeText(getApplicationContext(),ans1+","+ans2+","+ans3+","+ans4,Toast.LENGTH_SHORT).show();
        //Log.d("SRI","\t"+ans1+"\t"+ans2+"\t"+ans3+"\t"+ans4);

        /*progress=new ProgressDialog(this);
        progress.setMessage("Downloading Music");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.show();

        final int totalProgressTime = 5;
        final Thread t = new Thread() {
            @Override
            public void run() {
                int jumpTime = 0;

                while(jumpTime < totalProgressTime) {
                    try {
                        sleep(200);
                        jumpTime += 1;
                        progress.setProgress(jumpTime);
                    }
                    catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();*/
    showMap.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(flag == 1) {
                Intent intent = new Intent(getApplicationContext(), ShowMap.class);
                intent.putExtra("lat","38.122431");
                intent.putExtra("lang","-122.257036");
                startActivity(intent);
            }
            if(flag == 2) {
                Intent intent = new Intent(getApplicationContext(), ShowMap.class);
                intent.putExtra("lat","38.3113475");
                intent.putExtra("lang","-122.258191");
                startActivity(intent);
            }
            if(flag == 5) {
                Intent intent = new Intent(getApplicationContext(), ShowMap.class);
                intent.putExtra("lat","38.122310");
                intent.putExtra("lang","-122.257036");
                startActivity(intent);
            }
            if(flag == 6) {
                Intent intent = new Intent(getApplicationContext(), ShowMap.class);
                intent.putExtra("lat","41.2131788");
                intent.putExtra("lang","-124.0046276");
                startActivity(intent);
            }

            if(flag == 3 || flag == 4) {
                Toast.makeText(getApplicationContext(),"",Toast.LENGTH_SHORT).show();

            }

        }


    });



    }


    class Task extends AsyncTask<String, Integer, Boolean>{

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //dialog.cancel();
            dialog.setCancelable(true);
        }



        @Override
        protected Boolean doInBackground(String... params) {


            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
        @Override
        protected void onPostExecute(Boolean result) {

            if(result == true) {
                dialog.cancel();
                ll.setVisibility(View.VISIBLE);
                showMap.setVisibility(View.VISIBLE);

                if((ans1.equals("Happy") && ans2.equals("Not Sunny") && ans3.equals("Money")) && (ans4.equals("Young") || ans4.equals("Old")))
                {
                    //Toast.makeText(getApplicationContext(),"Go to Park",Toast.LENGTH_SHORT).show();

                    displaySelected.setText("Mood : "+ans1+"\t"+"Weather : "+ans2+"\t"+"Budget : "+ans3+"\t"+"Age : "+ans4);
                    doAction.setText("Why dont you Goto Restarent");
                    namer.setText("Jolibe Restarent");
                    imageView.setImageResource(R.drawable.restatrnt);
                    flag = 1;
                }
                if((ans1.equals("Happy") && ans2.equals("Sunny") && ans3.equals("Money")) && (ans4.equals("Young") || ans4.equals("Old")))
                {
                    //Toast.makeText(getApplicationContext(),"Go to Park",Toast.LENGTH_SHORT).show();
                    displaySelected.setText("Mood : "+ans1+"\t"+"Weather : "+ans2+"\t"+"Budget : "+ans3+"\t"+"Age : "+ans4);
                    doAction.setText("Why dont you Goto Movie");
                    namer.setText("RC Theater");
                    imageView.setImageResource(R.drawable.movie);
                    flag = 2;
                }
                if((ans1.equals("Happy") && ans2.equals("Sunny") && ans3.equals("No Money")) && (ans4.equals("Young") || ans4.equals("Old")))
                {
                    //Toast.makeText(getApplicationContext(),"Go to Park",Toast.LENGTH_SHORT).show();
                    displaySelected.setText("Mood : "+ans1+"\t"+"Weather : "+ans2+"\t"+"Budget : "+ans3+"\t"+"Age : "+ans4);
                    doAction.setText("Enjoy With your Family Members");
                    namer.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    flag = 3;
                }
                if((ans1.equals("Happy") && ans2.equals("Not Sunny") && ans3.equals("No Money")) && (ans4.equals("Young") || ans4.equals("Old")))
                {
                    //Toast.makeText(getApplicationContext(),"Go to Park",Toast.LENGTH_SHORT).show();
                    displaySelected.setText("Mood : "+ans1+"\t"+"Weather : "+ans2+"\t"+"Budget : "+ans3+"\t"+"Age : "+ans4);
                    doAction.setText("Enjoy With your Family Friends");
                    namer.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    flag = 4;
                }
                if((ans1.equals("UnHappy") && ans2.equals("Sunny") && ans3.equals("Money")) && (ans4.equals("Young") || ans4.equals("Old")))
                {
                    //Toast.makeText(getApplicationContext(),"Go to Park",Toast.LENGTH_SHORT).show();
                    displaySelected.setText("Mood : "+ans1+"\t"+"Weather : "+ans2+"\t"+"Budget : "+ans3+"\t"+"Age : "+ans4);
                    doAction.setText("Why dont you Goto Gym");
                    namer.setText("Planet Fitness");
                    imageView.setImageResource(R.drawable.imggym);
                    flag = 5;
                }
                if((ans1.equals("UnHappy") && ans2.equals("Not Sunny") && ans3.equals("No Money")) && (ans4.equals("Young") || ans4.equals("Old")))
                {
                    //Toast.makeText(getApplicationContext(),"Go to Park",Toast.LENGTH_SHORT).show();
                    displaySelected.setText("Mood : "+ans1+"\t"+"Weather : "+ans2+"\t"+"Budget : "+ans3+"\t"+"Age : "+ans4);
                    doAction.setText("Why dont you Goto Park");
                    namer.setText("Redwood National and State park");
                    imageView.setImageResource(R.drawable.park);
                    flag = 6;
                }

            }
            // super.onPostExecute(result);
        }
    }


    public void gotoHome(View view)
    {
        Intent intent = new Intent(this,HomePage.class);
        startActivity(intent);
    }



   /* public void gotoMap(View view)
    {
        if(flag == 1) {
            Intent intent = new Intent(this, ShowMap.class);
            intent.putExtra("lat","38.122431");
            intent.putExtra("lang","-122.257036");
            startActivity(intent);
        }
        if(flag == 2) {
            Intent intent = new Intent(this, ShowMap.class);
            intent.putExtra("lat","38.3113475");
            intent.putExtra("lang","-122.258191");
            startActivity(intent);
        }
        if(flag == 5) {
            Intent intent = new Intent(this, ShowMap.class);
            intent.putExtra("lat","38.122310");
            intent.putExtra("lang","-122.257036");
            startActivity(intent);
        }
        if(flag == 6) {
            Intent intent = new Intent(this, ShowMap.class);
            intent.putExtra("lat","41.2131788");
            intent.putExtra("lang","-124.0046276");
            startActivity(intent);
        }

        if(flag == 3 || flag == 4) {
            Toast.makeText(getApplicationContext(),"",Toast.LENGTH_SHORT).show();

        }

    }*/
}