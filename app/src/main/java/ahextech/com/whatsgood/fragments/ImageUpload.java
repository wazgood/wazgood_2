package ahextech.com.whatsgood.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ahextech.com.whatsgood.HomePage;
import ahextech.com.whatsgood.MainActivity;
import ahextech.com.whatsgood.R;
import ahextech.com.whatsgood.UserSessionManager;
import ahextech.com.whatsgood.fragments.FilePath;
import ahextech.com.whatsgood.fragments.ImageUpload;
import ahextech.com.whatsgood.util.PlaceAutocompleteAdapter;

public class ImageUpload extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{

    private static final int PICK_FILE_REQUEST = 1;
    private static final String TAG = ImageUpload.class.getSimpleName();
    private String selectedFilePath;
    private String SERVER_URL = "http://54.179.146.227/wazgood/public/user/image/data";
    ImageView ivAttachment;
    Button bUpload;
    TextView tvFileName;
    ProgressDialog dialog;
    //Button logout;
    UserSessionManager session;
    RequestQueue queue;
    // String url = "http://54.179.146.227/wazgood/public/user/logout";
    String key;
    //TextView result;

    MultiAutoCompleteTextView multi;
    Button Get;
    List<String> list;
    EditText comment;



    /*AutocompleteTxt Addresses*/
    protected GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;

    private AutoCompleteTextView mAutocompleteView;
    private LatLng latlong;
    double latitude;
    double longitude;
    String serverResponseMessage;
    String address,lat,lng,tags;
    String vale1,vale2;

    String x = "a";
    String y = "a";
    String z = "a";
    String a = "a";
    String b = "a";
    RequestQueue requestQueue;

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);
        //ivAttachment = (ImageView) findViewById(R.id.ivAttachment);
        bUpload = (Button) findViewById(R.id.b_upload);
        // tvFileName = (TextView) findViewById(R.id.tv_file_name);
        //ivAttachment.setOnClickListener(this);
        bUpload.setOnClickListener(this);
        queue = Volley.newRequestQueue(getApplicationContext());
        comment = (EditText) findViewById(R.id.tv_comment);
        requestQueue = Volley.newRequestQueue(this);

        //Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        //intent.addCategory(Intent.CATEGORY_OPENABLE);
       // intent.setType("*/*");
       // String[] mimetypes = {"image/*", "video/*"};
       // intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
       // startActivityForResult(Intent.createChooser(intent,"Choose File to Upload.."), PICK_FILE_REQUEST);




        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();


        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView)
                findViewById(R.id.autocomplete_places);

        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        // Retrieve the TextViews that will display details and attributions of the selected place.
        //mPlaceDetailsText = (TextView) findViewById(R.id.place_details);
        //mPlaceDetailsAttribution = (TextView) findViewById(R.id.place_attribution);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
        mAutocompleteView.setAdapter(mAdapter);




        session = new UserSessionManager(ImageUpload.this);

        HashMap<String, String> user = session.getUserDetails();
        // get name
        key = user.get(UserSessionManager.KEY_ACCSES);





        list = new ArrayList<String>();
        list.add("Adventure");
        list.add("Restaurant");
        list.add("Cycling");
        list.add("Bar");
        list.add("Hotel");
        list.add("Dinner");
        list.add("Lunch");
        list.add("Travel");
        list.add("Tour");
        list.add("Discover");
        list.add("Racing");
        list.add("Riding");
        list.add("Competition");

        multi = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextView1);
        //Get = (Button) findViewById(R.id.button1);

        ArrayAdapter<String> adp = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, list);

        multi.setAdapter(adp);

        multi.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        multi.setThreshold(1);
        //tags = multi.getText().toString();
        //Log.d("PKO",tags);

        /*Get.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String str = multi.getText().toString();



                Toast.makeText(getBaseContext(), str, Toast.LENGTH_LONG).show();

            }
        });*/


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(ImageUpload.this,response,Toast.LENGTH_LONG).show();
                        Log.d("AHEX",response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ImageUpload.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams()  throws AuthFailureError {
                Map<String,String> postDataParams = new HashMap<String, String>();
                // String req = "Bearer {"+key+"}";
                //postDataParams.put("Authorization", req);
                postDataParams.put("resource","Reso");
                postDataParams.put("type","image");
                postDataParams.put("address", "Ahex Hyd");
                postDataParams.put("latitude", "17.89656");
                postDataParams.put("longitude", "78.56986");
                postDataParams.put("tags", "Hotel");
                postDataParams.put("comments", "Good");
                postDataParams.put("fb_user_id","957285991045262");

                return postDataParams;
            }

        };


        requestQueue.add(stringRequest);



    }



    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(view.getApplicationWindowToken(),0);

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }

            final Place place = places.get(0);


            latlong = place.getLatLng();


            latitude = latlong.latitude;
            longitude = latlong.longitude;

            address = place.getAddress().toString();
            lat = String.valueOf(latitude);
            lng = String.valueOf(longitude);
            //lng = ""+longitude;

            Log.d("KAVITAM",""+address+","+lat+","+lng);
            // Toast.makeText(getApplicationContext(),""+latlong+"\n Latitude :"+latitude+"\n Longitude :"+longitude,Toast.LENGTH_LONG).show();

            final CharSequence thirdPartyAttribution = places.getAttributions();
            if (thirdPartyAttribution == null) {

            } else {

            }



            places.release();
        }
    };

    private static Spanned formatPlaceDetails(Resources res, LatLng latlong, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        //Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
        //websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }







    @Override
    public void onClick(View v) {
        /*if(v== ivAttachment){

            //on attachment icon click
            showFileChooser();
        }*/
        if(v== bUpload){

            //on upload button Click
           /* if(selectedFilePath != null){
                dialog = ProgressDialog.show(ImageUpload.this,"","Uploading File...",true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //creating new thread to handle Http Operations*/
                        vale1 = multi.getText().toString();
                        vale2 = comment.getText().toString();
                       // uploadFile();
                    /*}
                }).start();
            }else{
                Toast.makeText(ImageUpload.this,"Please choose a File First",Toast.LENGTH_SHORT).show();
            }
*/
        }
    }

    private void showFileChooser() {
       /* Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("video*//*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent,"Choose File to Upload.."),PICK_FILE_REQUEST);*/



        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimetypes = {"image/*", "video/*"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(Intent.createChooser(intent,"Choose File to Upload.."), PICK_FILE_REQUEST);
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == PICK_FILE_REQUEST){
                if(data == null){
                    //no data present
                    return;
                }


                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(this,selectedFileUri);
                Log.i(TAG,"Selected File Path:" + selectedFilePath);

                if(selectedFilePath != null && !selectedFilePath.equals("")){

                    //tvFileName.setText(selectedFilePath);
                    Toast.makeText(getApplicationContext(),"File Selected ",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this,"Cannot upload file to server",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
*/
    //android upload file to server
    public void uploadFile(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(ImageUpload.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ImageUpload.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams()  throws AuthFailureError {
                Map<String,String> postDataParams = new HashMap<String, String>();
               // String req = "Bearer {"+key+"}";
                //postDataParams.put("Authorization", req);
                postDataParams.put("resource","Reso");
                postDataParams.put("type","image");
                postDataParams.put("address", address);
                postDataParams.put("latitude", lat);
                postDataParams.put("longitude", lng);
                postDataParams.put("tags", vale1);
                postDataParams.put("comments", vale2);

                return postDataParams;
            }

        };


        requestQueue.add(stringRequest);


       /* int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead,bytesAvailable,bufferSize;
        byte[] buffer;
        int maxBufferSize = 300000 * 1024 * 1024;
       // File selectedFile = new File(selectedFilePath);


       // String[] parts = selectedFilePath.split("/");
        //final String fileName = parts[parts.length-1];

        *//*if (!selectedFile.isFile()){
            dialog.dismiss();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // tvFileName.setText("Source File Doesn't Exist: " + selectedFilePath);
                }
            });
            return 0;
        }else{*//*
            try{
              //  FileInputStream fileInputStream = new FileInputStream(selectedFile);
                HashMap<String,String> postDataParams=new HashMap<String, String>();

                postDataParams.put("address", address);
                postDataParams.put("latitude", lat);
                postDataParams.put("longitude", lng);
                postDataParams.put("tags", vale1);
                postDataParams.put("comments", vale2);
               // postDataParams.put("file",selectedFilePath);

                URL url = new URL(SERVER_URL+"?"+getPostDataString(postDataParams));
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Authorization", "Bearer {" + key + "}");
                //connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
               // connection.setRequestProperty("file", selectedFilePath);
                *//*connection.setRequestProperty("address",address);
                connection.setRequestProperty("latitude",lat);
                connection.setRequestProperty("longitude",lng);
                connection.setRequestProperty("tags",vale1);
                connection.setRequestProperty("comments",vale2);*//*



                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());
                ;
                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

              *//*  //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer,0,bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0){
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer,0,bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
                }
*//*
                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                serverResponseMessage = connection.getResponseMessage();

                *//*InputStream is = connection.getInputStream();
                byte[] b = new byte[1024];
                StringBuffer buffer1=new StringBuffer("");
                while ( is.read(b) != -1)
                    buffer1.append(new String(b));*//*
               // connection.disconnect();

                Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode+"  ----");

                //response code of 200 indicates the server status OK
                if(serverResponseCode == 200){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // tvFileName.setText("File Upload completed");
                            Intent intent = new Intent(ImageUpload.this, HomePage.class);
                            startActivity(intent);
                        }
                    });
                }
                if(serverResponseCode == 404){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //tvFileName.setText("File not uploaded");
                            Log.d("LAX",serverResponseMessage);
                        }
                    });
                }

                //closing the input and output streams
                //fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();



            } *//*catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ImageUpload.this,"File Not Found",Toast.LENGTH_SHORT).show();
                    }
                });
            }*//* catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(ImageUpload.this, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(ImageUpload.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }    return serverResponseCode;*/
        }

    //}
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}