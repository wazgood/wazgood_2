package ahextech.com.whatsgood.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.android.gms.games.video.Video;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ahextech.com.whatsgood.DisplayImage;
import ahextech.com.whatsgood.DisplayVideo;
import ahextech.com.whatsgood.HomePage;
import ahextech.com.whatsgood.MainActivity;
import ahextech.com.whatsgood.R;
import ahextech.com.whatsgood.UserSessionManager;
import ahextech.com.whatsgood.adapters.TrendringAdapter;
import ahextech.com.whatsgood.holders.TrendringViewHolder;
import ahextech.com.whatsgood.models.TrendringList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Trending extends Fragment implements TrendringViewHolder.MyViewHolder,View.OnClickListener {
    //Button logout;
    UserSessionManager session;
    RequestQueue queue;
    public Button btnClosePopup;
    public ImageView imageDisplay;
    public VideoView videoDisplay;

    public ProgressDialog pDialog;
   // String url = "http://54.179.146.227/wazgood/public/user/logout";
    String key;
    //TextView result;

    public PopupWindow popUpWindow;
    ArrayList<TrendringList> topicList;
    TrendringAdapter adapter;
    private TextView Train;
    ProgressDialog progressDialog;
    private static final int PICK_FILE_REQUEST = 1;
   // private static final String TAG = MainActivity.class.getSimpleName();
    private String selectedFilePath;
    //private String SERVER_URL = "http://54.179.146.227/wazgood/public/user/file";
    //private String SERVER_URL = "http://54.179.146.227/wazgood/public/all";
    Button select_galery,select_camera;






    public Trending() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        queue = Volley.newRequestQueue(getContext());

        // Bundle bundle = getIntent().getExtras();

        session = new UserSessionManager(getActivity());
        View view = inflater.inflate(R.layout.fragment_trending, container, false);
        HashMap<String, String> user = session.getUserDetails();
               // get name
         key = user.get(UserSessionManager.KEY_ACCSES);
        topicList = new ArrayList<TrendringList>();

        final RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
       // camera = (ImageView)view.findViewById(R.id.camera);
       // camera.setOnClickListener(this);
        select_galery = (Button)view.findViewById(R.id.selectgalery);
        select_camera = (Button)view.findViewById(R.id.selectcemera);
        select_galery.setOnClickListener(this);
        select_camera.setOnClickListener(this);
        adapter = new TrendringAdapter(this,R.layout.row_trendring_layout,topicList);
        recyclerView.setAdapter(adapter);
        new JSONAsyncTask().execute("http://54.179.146.227/wazgood/public/all");
        return view;



    }

    @Override
    public void onClick(View v) {
        /*if(v==camera){

            Intent intent = new Intent(getActivity(),ImageUpload.class);
            startActivity(intent);
        }*/
            if(v == select_galery)
            {
                Intent intent = new Intent(getActivity(),ImageUpload.class);
                startActivity(intent);
            }

        }


    class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Please wait .....");

            progressDialog.show();
            progressDialog.setCancelable(false);

        }

        @Override
        protected Boolean doInBackground(String... params) {


            HttpGet httpGet = new HttpGet(params[0]);
            String req = "Bearer {"+key+"}";
           // httpGet.setHeader("Authorization",req);
            HttpClient httpClient = new DefaultHttpClient();


            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity entity = response.getEntity();

                String data = EntityUtils.toString(entity);
                Log.d("SVKP",data);
                JSONObject jsonObject = new JSONObject(data);

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for(int i = 0 ; i < jsonArray.length() ; i++)
                {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    TrendringList tl = new TrendringList();

                    tl.setResource("http://54.179.146.227/wazgood/storage/uploads/"+object1.getString("resource"));
                    //tl.setResource(object1.getString("resource"));
                    tl.setFile_type(object1.getString("type"));
                    Log.d("RAMU",""+object1.getString("resource"));
                   // tl.setThumbnail(object1.getString("thumbnail"));
                    tl.setComments(object1.getString("comments"));
                    tl.setTags(object1.getString("tags"));
                    tl.setLatitude(object1.getString("latitude"));
                    tl.setLongitude(object1.getString("longitude"));
                    tl.setAddress(object1.getString("address"));
                    tl.setCreated_at(object1.getString("created_at"));
                    tl.setImageLink(object1.getString("dummy_image"));
                    tl.setPost_id(object1.getString("app_id"));
                    tl.setPost_name(object1.getString("app_name"));

                    topicList.add(tl);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            adapter.notifyDataSetChanged();

            if(aBoolean == false)
            {
                progressDialog.cancel();


            }
        }
    }



    @Override
    public void cardViewListener(int position) {


        String type = topicList.get(position).getFile_type();
        String resourceUrl = topicList.get(position).getResource();
        if(type.equals("video")) {
            Intent intent = new Intent(getActivity(), DisplayVideo.class);
            intent.putExtra("pos", position);
            intent.putExtra("item", topicList.get(position));
            startActivity(intent);
            /*String file_type = topicList.get(position).getFile_type();
            initiatePopupWindow(file_type,resourceUrl);*/
        }
        else if(type.equals("image"))
        {
            /*Intent intent = new Intent(getActivity(), DisplayImage.class);
            intent.putExtra("pos", position);
            intent.putExtra("item", topicList.get(position));
            startActivity(intent);*/
            String file_type = topicList.get(position).getFile_type();
            initiatePopupWindow(file_type,resourceUrl);
        }
    }


    private void initiatePopupWindow(String fileType,String resourceUrl) {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup, (ViewGroup) getActivity().findViewById(R.id.popup_element));
            popUpWindow = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            //popUpWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
            //popUpWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

            popUpWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
            imageDisplay = (ImageView)layout.findViewById(R.id.ImageView);
            videoDisplay = (VideoView)layout.findViewById(R.id.VideoView);

            if (fileType.equals("image"))
            {
// We need to get the instance of the LayoutInflater
                videoDisplay.setVisibility(View.INVISIBLE);
                Picasso.with(getActivity()).load(resourceUrl).placeholder(R.drawable.loading).into(imageDisplay);
            }

            else if(fileType.equals("image"))
            {
                imageDisplay.setVisibility(View.INVISIBLE);

                pDialog = new ProgressDialog(getActivity());
                // Set progressbar title
                // pDialog.setTitle("Please wait......");
                // Set progressbar message
                pDialog.setMessage("Buffering...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                // Show progressbar
                pDialog.show();

                try {
                    // Start the MediaController
                    MediaController mediacontroller = new MediaController(getActivity());
                    mediacontroller.setAnchorView(videoDisplay);
                    // Get the URL from String VideoURL

                    Uri video = Uri.parse(resourceUrl);
                    videoDisplay.setMediaController(mediacontroller);

                    videoDisplay.setVideoURI(video);

                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }

                videoDisplay.requestFocus();
                videoDisplay.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    // Close the progress bar and play the video
                    public void onPrepared(MediaPlayer mp) {
                        pDialog.dismiss();
                        videoDisplay.start();
                    }
                });


            }

            btnClosePopup.setOnClickListener(cancel_button_click_listener);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener cancel_button_click_listener = new View.OnClickListener() {
        public void onClick(View v) {
            popUpWindow.dismiss();

        }
    };



}
