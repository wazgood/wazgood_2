package ahextech.com.whatsgood.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ahextech.com.whatsgood.Decision;
import ahextech.com.whatsgood.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Suggestions extends Fragment implements AdapterView.OnItemSelectedListener{
    public Spinner spinner,answerSpinner;
    List<String> questionList;
    ArrayAdapter<String> dataAdapter,dataAdapterA;
    StringBuffer stringBuffer;
    String answ1,answ2,answ3,answ4;
    public ImageView image1,image2,image3,image4;
    TextView display,textViewOne,textViewTwo,textViewThree,textViewFour;
    Button submit;

    public Suggestions() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_suggestions, container, false);

        spinner = (Spinner)view.findViewById(R.id.spinner);
        answerSpinner = (Spinner)view.findViewById(R.id.spinnerAnsw);
        submit = (Button)view.findViewById(R.id.submitButton);
        //display = (TextView) findViewById(R.id.displayData);
        textViewOne = (TextView)view.findViewById(R.id.textviewOne);
        textViewTwo = (TextView)view.findViewById(R.id.textviewTwo);
        textViewThree = (TextView)view.findViewById(R.id.textviewThree);
        textViewFour = (TextView)view.findViewById(R.id.textviewFour);
        image1 = (ImageView)view.findViewById(R.id.image1);
        image2 = (ImageView)view.findViewById(R.id.image2);
        image3 = (ImageView)view.findViewById(R.id.image3);
        image4 = (ImageView)view.findViewById(R.id.image4);
        spinner.setOnItemSelectedListener(this);
        answerSpinner.setOnItemSelectedListener(this);
        stringBuffer = new StringBuffer();
        questionList = new ArrayList<String>();
        questionList.add("Select Question");
        questionList.add("Mood");
        questionList.add("Weather");
        questionList.add("Budget");
        questionList.add("Age");

        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, questionList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        dataAdapter.notifyDataSetChanged();



        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedQuestion = parent.getItemAtPosition(position).toString();
        // Log.d("SRIPURUSHOTHAM",""+questionList.size());
        if(questionList.size() == 1)
        {
            spinner.setVisibility(View.GONE);
            submit.setVisibility(View.VISIBLE);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getApplicationContext(),display.getText().toString(),Toast.LENGTH_SHORT).show();
                    // Toast.makeText(getApplicationContext(),answ1+answ2+answ3+answ4,Toast.LENGTH_SHORT).show();
                    //Mood,Weather,Budget,Age,answ1,answ2,answ3,answ4;
                    Intent intent = new Intent(getActivity(),Decision.class);
                    intent.putExtra("Mood",answ1);
                    intent.putExtra("Weather",answ2);
                    intent.putExtra("Budget",answ3);
                    intent.putExtra("Age",answ4);
                    startActivity(intent);
                }
            });
        }
        else {

            if (position > 0) {

                // Toast.makeText(getApplicationContext(), selectedQuestion, Toast.LENGTH_SHORT).show();

                if (selectedQuestion.equals("Mood")) {
                    if (answerSpinner.getVisibility() == View.VISIBLE && questionList.size() > 1) {
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Happy");
                        list.add("UnHappy");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Mood : " + selectedAnswMood+"\n");

                                    answ1 = selectedAnswMood;
                                    questionList.remove("Mood");
                                    // Log.d("9030", "" + questionList);

                                    if(selectedAnswMood.equals("Happy"))
                                    {
                                        textViewOne.setVisibility(View.VISIBLE);
                                        textViewOne.setTextColor(Color.parseColor("#4caf50"));
                                        textViewOne.setTextSize(20);

                                        textViewOne.setText("Mood : "+selectedAnswMood);
                                        image1.setVisibility(View.VISIBLE);
                                        image1.setImageResource(R.drawable.happy);
                                    }
                                    else if(selectedAnswMood.equals("UnHappy"))
                                    {
                                        //f44336
                                        textViewOne.setVisibility(View.VISIBLE);
                                        textViewOne.setTextColor(Color.parseColor("#f44336"));
                                        textViewOne.setTextSize(20);

                                        textViewOne.setText("Mood : "+selectedAnswMood);
                                        image1.setVisibility(View.VISIBLE);
                                        image1.setImageResource(R.drawable.unhappy);
                                    }

                                    dataAdapter.notifyDataSetChanged();
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);

                                } else {

                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        answerSpinner.setVisibility(View.VISIBLE);
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Happy");
                        list.add("UnHappy");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();

                                    stringBuffer.append(" Mood : " + selectedAnswMood+"\n");

                                    answ1 = selectedAnswMood;
                                    questionList.remove("Mood");
                                    if(selectedAnswMood.equals("Happy"))
                                    {
                                        textViewOne.setVisibility(View.VISIBLE);
                                        textViewOne.setTextColor(Color.parseColor("#4caf50"));
                                        textViewOne.setTextSize(20);

                                        textViewOne.setText("Mood : "+selectedAnswMood);
                                        image1.setVisibility(View.VISIBLE);
                                        image1.setImageResource(R.drawable.happy);
                                    }
                                    else if(selectedAnswMood.equals("UnHappy"))
                                    {
                                        //f44336
                                        textViewOne.setVisibility(View.VISIBLE);
                                        textViewOne.setTextColor(Color.parseColor("#f44336"));
                                        textViewOne.setTextSize(20);

                                        textViewOne.setText("Mood : "+selectedAnswMood);
                                        image1.setVisibility(View.VISIBLE);
                                        image1.setImageResource(R.drawable.unhappy);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    dataAdapter.notifyDataSetChanged();
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }

                if (selectedQuestion.equals("Weather")) {
                    if (answerSpinner.getVisibility() == View.VISIBLE) {
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Sunny");
                        list.add("Not Sunny");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Weather : " + selectedAnswMood+"\n");

                                    answ2 = selectedAnswMood;
                                    questionList.remove("Weather");

                                    dataAdapter.notifyDataSetChanged();
                                    if(selectedAnswMood.equals("Sunny"))
                                    {
                                        textViewTwo.setVisibility(View.VISIBLE);
                                        textViewTwo.setTextColor(Color.parseColor("#4caf50"));
                                        textViewTwo.setTextSize(20);

                                        textViewTwo.setText("Weather : "+selectedAnswMood);
                                        image2.setVisibility(View.VISIBLE);
                                        image2.setImageResource(R.drawable.sunny);
                                    }
                                    else if(selectedAnswMood.equals("Not Sunny"))
                                    {
                                        textViewTwo.setVisibility(View.VISIBLE);
                                        textViewTwo.setTextColor(Color.parseColor("#f44336"));
                                        textViewTwo.setTextSize(20);

                                        textViewTwo.setText("Weather : "+selectedAnswMood);
                                        image2.setVisibility(View.VISIBLE);
                                        image2.setImageResource(R.drawable.cloud);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        answerSpinner.setVisibility(View.VISIBLE);
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Sunny");
                        list.add("Not Sunny");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Weather : " + selectedAnswMood+"\n");

                                    answ2 = selectedAnswMood;
                                    questionList.remove("Weather");
                                    dataAdapter.notifyDataSetChanged();
                                    if(selectedAnswMood.equals("Sunny"))
                                    {
                                        textViewTwo.setVisibility(View.VISIBLE);
                                        textViewTwo.setTextColor(Color.parseColor("#4caf50"));
                                        textViewTwo.setTextSize(20);

                                        textViewTwo.setText("Weather : "+selectedAnswMood);
                                        image2.setVisibility(View.VISIBLE);
                                        image2.setImageResource(R.drawable.sunny);
                                    }
                                    else if(selectedAnswMood.equals("Not Sunny"))
                                    {
                                        textViewTwo.setVisibility(View.VISIBLE);
                                        textViewTwo.setTextColor(Color.parseColor("#f44336"));
                                        textViewTwo.setTextSize(20);

                                        textViewTwo.setText("Weather : "+selectedAnswMood);
                                        image2.setVisibility(View.VISIBLE);
                                        image2.setImageResource(R.drawable.cloud);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }
                if (selectedQuestion.equals("Budget")) {
                    if (answerSpinner.getVisibility() == View.VISIBLE) {
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Money");
                        list.add("No Money");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Budget : " + selectedAnswMood+"\n");

                                    answ3 = selectedAnswMood;
                                    questionList.remove("Budget");
                                    dataAdapter.notifyDataSetChanged();
                                    if(selectedAnswMood.equals("Money"))
                                    {
                                        textViewThree.setVisibility(View.VISIBLE);
                                        textViewThree.setTextColor(Color.parseColor("#4caf50"));
                                        textViewThree.setTextSize(20);

                                        textViewThree.setText("Budget : "+selectedAnswMood);
                                        image3.setVisibility(View.VISIBLE);
                                        image3.setImageResource(R.drawable.money);
                                    }
                                    else if(selectedAnswMood.equals("No Money"))
                                    {
                                        textViewThree.setVisibility(View.VISIBLE);
                                        textViewThree.setTextColor(Color.parseColor("#f44336"));
                                        textViewThree.setTextSize(20);

                                        textViewThree.setText("Budget : "+selectedAnswMood);
                                        image3.setVisibility(View.VISIBLE);
                                        image3.setImageResource(R.drawable.nomoney);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        answerSpinner.setVisibility(View.VISIBLE);
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Money");
                        list.add("No Money");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Budget : " + selectedAnswMood+"\n");

                                    answ3 = selectedAnswMood;
                                    questionList.remove("Budget");
                                    dataAdapter.notifyDataSetChanged();
                                    if(selectedAnswMood.equals("Money"))
                                    {
                                        textViewThree.setVisibility(View.VISIBLE);
                                        textViewThree.setTextColor(Color.parseColor("#4caf50"));
                                        textViewThree.setTextSize(20);

                                        textViewThree.setText("Budget : "+selectedAnswMood);
                                        image3.setVisibility(View.VISIBLE);
                                        image3.setImageResource(R.drawable.money);
                                    }
                                    else if(selectedAnswMood.equals("No Money"))
                                    {
                                        textViewThree.setVisibility(View.VISIBLE);
                                        textViewThree.setTextColor(Color.parseColor("#f44336"));
                                        textViewThree.setTextSize(20);

                                        textViewThree.setText("Budget : "+selectedAnswMood);
                                        image3.setVisibility(View.VISIBLE);
                                        image3.setImageResource(R.drawable.nomoney);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }
                if (selectedQuestion.equals("Age")) {
                    if (answerSpinner.getVisibility() == View.VISIBLE) {
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Young");
                        list.add("Old");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Age : " + selectedAnswMood+"\n");

                                    answ4 = selectedAnswMood;
                                    questionList.remove("Age");
                                    dataAdapter.notifyDataSetChanged();
                                    if(selectedAnswMood.equals("Young"))
                                    {
                                        //display.setTextColor(Color.parseColor("#4caf50"));
                                        textViewFour.setVisibility(View.VISIBLE);
                                        textViewFour.setTextSize(20);
                                        textViewFour.setTextColor(Color.parseColor("#4caf50"));
                                        textViewFour.setText("Age : "+selectedAnswMood);
                                        image4.setVisibility(View.VISIBLE);
                                        image4.setImageResource(R.drawable.young);
                                    }
                                    else if(selectedAnswMood.equals("Old"))
                                    {
                                        // display.setTextColor(Color.parseColor("#f44336"));
                                        textViewFour.setVisibility(View.VISIBLE);
                                        textViewFour.setTextSize(20);
                                        textViewFour.setTextColor(Color.parseColor("#f44336"));
                                        textViewFour.setText("Age : "+selectedAnswMood);
                                        image4.setVisibility(View.VISIBLE);
                                        image4.setImageResource(R.drawable.old);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        answerSpinner.setVisibility(View.VISIBLE);
                        List<String> list = new ArrayList<String>();
                        list.add("Select Answer");
                        list.add("Young");
                        list.add("Old");
                        dataAdapterA = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_item, list);
                        dataAdapterA.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dataAdapterA.notifyDataSetChanged();
                        answerSpinner.setAdapter(dataAdapterA);
                        answerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selectedAnswMood = parent.getItemAtPosition(position).toString();
                                if (position > 0) {
                                    // Toast.makeText(getApplicationContext(), selectedAnswMood, Toast.LENGTH_SHORT).show();
                                    stringBuffer.append(" Age : " + selectedAnswMood+"\n");

                                    answ4 = selectedAnswMood;
                                    questionList.remove("Age");
                                    dataAdapter.notifyDataSetChanged();
                                    if(selectedAnswMood.equals("Young"))
                                    {
                                        //display.setTextColor(Color.parseColor("#4caf50"));
                                        textViewFour.setVisibility(View.VISIBLE);
                                        textViewFour.setTextSize(20);
                                        textViewFour.setTextColor(Color.parseColor("#4caf50"));
                                        textViewFour.setText("Age : "+selectedAnswMood);
                                        image4.setVisibility(View.VISIBLE);
                                        image4.setImageResource(R.drawable.young);
                                    }
                                    else if(selectedAnswMood.equals("Old"))
                                    {
                                        //display.setTextColor(Color.parseColor("#f44336"));
                                        textViewFour.setVisibility(View.VISIBLE);
                                        textViewFour.setTextSize(20);
                                        textViewFour.setTextColor(Color.parseColor("#f44336"));
                                        textViewFour.setText("Age : "+selectedAnswMood);
                                        image4.setVisibility(View.VISIBLE);
                                        image4.setImageResource(R.drawable.old);
                                    }
                                    // Log.d("9030", "" + questionList);
                                    spinner.setAdapter(dataAdapter);
                                    answerSpinner.setVisibility(View.GONE);
                                } else {

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }
            }
        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

