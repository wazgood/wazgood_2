package ahextech.com.whatsgood.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import ahextech.com.whatsgood.R;

/**
 * Created by androiddeveloper on 6/5/16.
 */
public class TrendringViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public ImageView displayVideo,display_profilepic;
    public TextView profile_name,post_file_address,commints;

    MyViewHolder context;

    public TrendringViewHolder(View itemView, MyViewHolder context) {
        super(itemView);
        this.context = context;
        itemView.setOnClickListener(this);
        displayVideo = (ImageView)itemView.findViewById(R.id.videoView);
        display_profilepic = (ImageView)itemView.findViewById(R.id.profilepic);
        profile_name = (TextView)itemView.findViewById(R.id.profileName);
        post_file_address = (TextView)itemView.findViewById(R.id.post_address);
        commints = (TextView)itemView.findViewById(R.id.users_comments);
    }

    @Override
    public void onClick(View view) {
        // Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition()+"  ", Toast.LENGTH_SHORT).show();
        context.cardViewListener(getPosition());
    }
    public interface MyViewHolder
    {
        void cardViewListener(int position);

    }
}
