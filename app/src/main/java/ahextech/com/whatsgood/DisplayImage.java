package ahextech.com.whatsgood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import ahextech.com.whatsgood.models.TrendringList;

public class DisplayImage extends AppCompatActivity {
    TrendringList tl;
    ImageView imageDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_image);
        imageDisplay = (ImageView) findViewById(R.id.displayImage);
        Bundle extras = getIntent().getExtras();
        tl = (TrendringList) extras.getSerializable("item");

        Toast.makeText(getApplicationContext(),tl.getResource(),Toast.LENGTH_SHORT).show();
       String x = tl.getResource();
        Log.d("AHEX",""+tl.getResource());

        Picasso.with(this)
                .load(tl.getResource())
                .placeholder(R.drawable.loading)
                .into(imageDisplay);
    }

    public void gotoHomefromDisplayImage(View view) {
        Intent intent = new Intent(this, HomePage.class);
        startActivity(intent);
    }
}
