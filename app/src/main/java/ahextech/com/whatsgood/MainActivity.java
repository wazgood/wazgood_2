package ahextech.com.whatsgood;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Arrays;
import java.util.HashMap;

import ahextech.com.whatsgood.util.ConnectionDetector;

public class MainActivity extends AppCompatActivity {
    private static final String REGISTER_URL = "http://54.179.146.227/wazgood/public/user/facebook";
    CallbackManager callbackManager;
    LoginButton loginButton;
    UserSessionManager session;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;

    String verified_data,fb_user_id_data,email_data,name_data,gender_data;
   // SharedPreferences sharedpreferences;
    String res;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        facebookSDKInitialize();
        setContentView(R.layout.activity_main);
       // sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        session = new UserSessionManager(getApplicationContext());
        try
        {
            PackageInfo info = getPackageManager().getPackageInfo("ahextech.com.whatsgood", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",Base64.encodeToString(md.digest(), Base64.DEFAULT));
                //System.out.println("KEY HASH: "+Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
           // Log.d("KeyHash:",e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            //Log.d("KeyHash:",e.getMessage());
        }
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        //Log.d("Success", ""+loginResult);

                        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                JSONObject json = response.getJSONObject();
                              //  Log.d("999999999",json+"");

                                if(json != null){
                                    try {


                                        verified_data = json.getString("verified");
                                        fb_user_id_data = json.getString("id");
                                        email_data = json.getString("email");
                                        name_data = json.getString("name");
                                        gender_data = json.getString("gender");
                                        // String text = json.getString("id")+"\n"+"Name : "+json.getString("name")+"\n Gender : "+json.getString("gender")+"\n Email : "+json.getString("email")+"\n Profile link : "+json.getString("link")+"\n  Verified : "+json.getString("verified");
                                        register(verified_data,fb_user_id_data,email_data,name_data,gender_data);



                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,gender,link,email,verified,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(MainActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
       // Log.e("data",data.toString());


    }

    private void facebookSDKInitialize() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }


    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }
    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    private void register(String verified,String fb_user_id,String email,String name,String gender) {
        class RegisterUser extends AsyncTask<String, Void, String> {
            Dialog loading;
            RegisterRequest ruc = new RegisterRequest();


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.d("Iam Coming","Iam coming");
                loading=new Dialog(MainActivity.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                loading.setContentView(R.layout.loading_screen);
                loading.show();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                res = s;

                /*SharedPreferences.Editor editor = sharedpreferences.edit();
                if(!s.isEmpty()) {
                    editor.putString(AccesToken, s);
                    editor.putBoolean(isRegistered,true);
                    editor.commit();

                    Intent intent = new Intent(MainActivity.this,HomePage.class);
                    intent.putExtra("id",s);
                    startActivity(intent);
                }*/

                session.createUserLoginSession(res);
                Intent intent = new Intent(MainActivity.this,HomePage.class);
                //intent.putExtra("id",s);
                startActivity(intent);
                loading.dismiss();

                //  Log.d("RESPONCEAHEX",s);
                // Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(String... params) {



                HashMap<String, String> data = new HashMap<String,String>();
                data.put("verified",params[0]);
                data.put("fb_user_id",params[1]);
                data.put("email",params[2]);
                data.put("name",params[3]);
                data.put("gender",params[4]);

                String result = ruc.sendPostRequest(REGISTER_URL,data);
              //  Log.d("AHEX",result);
                return  result;
            }
        }

        RegisterUser ru = new RegisterUser();
        ru.execute(verified,fb_user_id,email,name,gender);
    }

    /*public void signinWithFacebook(View view) {
       // if(!sharedpreferences.getBoolean(isRegistered,false)) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends","email"));
        *//*}
        else
        {
            Intent intent = new Intent(MainActivity.this,HomePage.class);
            intent.putExtra("id",sharedpreferences.getString(AccesToken,""));
            startActivity(intent);
        }*//*


    }*/

    public void signinwithFacebook(View view) {
        if (isInternetPresent) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        }
        else {
            showAlertDialog(MainActivity.this, "No Internet Connection",
                    "Please Check Your Internet Connection", false);
        }
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);


        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.sucess : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}